#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BR 2

 struct zaposlenici{

    char jbmg[13],brojLicne[9],prezimeIme[40];
    int regBroj,zaposlenNaNeodr;
    float netoPlata;
};
struct zaposleniciAzurirano{
    char jbmg[13];
    int regBroj;
    float netoPlata;
};
void zamijeni(struct zaposlenici *pok1,struct zaposlenici *pok2){
    struct zaposlenici temp;
    temp = *pok1;
    *pok1=*pok2;
    *pok2=temp;
}
void sortirajBubble(struct zaposlenici niz[],int n){
    int i,j,sortiran;

    for(i=n;i>0;i--){
        sortiran=1;
        for(j=1;j<i;j++){
            if(niz[j-1].prezimeIme>niz[j].prezimeIme){
                zamijeni(&niz[j-1],&niz[j]);
                sortiran=0;
                }
        }

        if(sortiran)
            break;
    }
}
void sortirajSelection(struct zaposleniciAzurirano niz[],int n){
    int i,j;

    for(i=1;i<n;i++){
        j=i;
        while(j>0 && niz[j-1].regBroj>niz[j].regBroj){
            zamijeni(&niz[j-1],&niz[j]);
            j--;
            }
    }

}
int pretrazivanjeBinarno(int kljuc, struct zaposleniciAzurirano niz[],int donji, int gornji){
    int index=-1,sredina;

     while(donji<=gornji){
        sredina=(donji+gornji)/2;
        if(niz[sredina].regBroj==kljuc){
            index=sredina;
            return index;
        }else if(niz[sredina].regBroj<kljuc){
            donji=sredina+1;
        }else{
            gornji=sredina-1;
        }
    }

    return index;
}

int main()
{
    struct zaposlenici niz[BR],niz2[BR];
    struct zaposleniciAzurirano nizA[BR],niz2A[BR];
    int i,donji,gornji,sredina,index,kljuc;
    FILE *dat;
    //UNOS ZAPOSLENIKA ZA DATOTEKU Zapsolenici.dat
    for(i=0;i<BR;i++){
        printf("Unos %d. zaposlenika \n",i+1);
        printf("JBMG: ");
        scanf("%s",&niz[i].jbmg);
        printf("Broj licne: ");
        scanf("%s",&niz[i].brojLicne);
        printf("Prezime i ime (bez razmaka): ");

        scanf("%s",&niz[i].prezimeIme);

        printf("Registarski broj: ");
        scanf("%d",&niz[i].regBroj);
        printf("Da li je zaposlen na neodredjeno (1 da, 0 ne) ");
        scanf("%d",&niz[i].zaposlenNaNeodr);
        printf("Neto plata: ");
        scanf("%f",&niz[i].netoPlata);
    }
    //UNOS ZAPOSLENIKA ZA DATOTEKU 'Zaposlenici-Azurirano.txt'
     for(i=0;i<BR;i++){
        printf("Unos %d. zaposlenika \n",i+1);
        printf("JBMG: ");
        scanf("%s",&nizA[i].jbmg);
        printf("Registarski broj: ");
        scanf("%d",&nizA[i].regBroj);
        printf("Izmjenjena neto plata: ");
        scanf("%f",&nizA[i].netoPlata);
    }
    //KREIRANJE BINARNE DATOTEKE Zaposlenici.dat
    dat= fopen("Zaposlenici.dat","wb");
    fwrite(&niz[0],sizeof(niz),1,dat);
    fclose(dat);
    //KREIRANJE DATOTEKE 'Zaposlenici-Azurirano.txt'
    dat= fopen("Zaposlenici-Azurirano.txt","w");
    fwrite(&nizA[0],sizeof(nizA),1,dat);
    fclose(dat);
    //*****************************************
    //*****************************************
    //UCITAVANJE BINARNE DATOTEKE Zaposlenici.dat U niz2
    dat= fopen("Zaposlenici.dat","rb");
    fread(&niz2[0],sizeof(niz2),1,dat);
    fclose(dat);
    //UCITAVANJE DATOTEKE 'Zaposlenici-Azurirano.txt U niz2A'
    dat= fopen("Zaposlenici-Azurirano.txt","r");
    fread(&niz2A[0],sizeof(niz2A),1,dat);
    fclose(dat);
    //SORTIRANJE niz2 i niz2A
    sortirajBubble(niz2,BR);
    sortirajSelection(niz2A,BR);
    //ISPIS PRVOG NIZA
    printf("\n\nIspis datoteke Zaposlenici.dat \n\n");
    for(i=0;i<BR;i++){
        printf("Ispis %d. zaposlenika \n",i+1);
        printf("JBMG: ");
        printf("%s \n",niz[i].jbmg);
        printf("Broj licne: ");
        printf("%s \n",niz[i].brojLicne);
        printf("Prezime i ime): ");

        printf("%s \n",niz[i].prezimeIme);

        printf("Registarski broj: ");
        printf("%d \n",niz[i].regBroj);
        printf("Da li je zaposlen na neodredjeno (1 da, 0 ne) ");
        printf("%d \n",niz[i].zaposlenNaNeodr);
        printf("Neto plata: ");
        printf("%lf \n",niz[i].netoPlata);
    }
    printf("\n\nIspis datoteke Zaposlenici-Azurirano.txt \n\n");
    for(i=0;i<BR;i++){
        printf("Ispis %d. zaposlenika \n",i+1);
        printf("JBMG: ");
        printf("%s \n",nizA[i].jbmg);
        printf("Registarski broj: ");
        printf("%d \n",nizA[i].regBroj);
        printf("Izmjenjena neto plata: ");
        printf("%lf \n",nizA[i].netoPlata);
    }
    //ISPIS DRUGOG NIZA (SORTIRANOG NIZA)
    printf("\n\nIspis sortirane datoteke Zaposlenici.dat \n\n");
    for(i=0;i<BR;i++){
        printf("Ispis %d. zaposlenika \n",i+1);
        printf("JBMG: ");
        printf("%s \n",niz2[i].jbmg);
        printf("Broj licne: ");
        printf("%s \n",niz2[i].brojLicne);
        printf("Prezime i ime): ");

        printf("%s \n",niz2[i].prezimeIme);

        printf("Registarski broj: ");
        printf("%d \n",niz2[i].regBroj);
        printf("Da li je zaposlen na neodredjeno (1 da, 0 ne) ");
        printf("%d \n",niz2[i].zaposlenNaNeodr);
        printf("Neto plata: ");
        printf("%lf \n",niz2[i].netoPlata);
    }
    printf("\n\nIspis datoteke Zaposlenici-Azurirano.txt sortirano \n\n");
    for(i=0;i<BR;i++){
        printf("Ispis %d. zaposlenika \n",i+1);
        printf("JBMG: ");
        printf("%s \n",niz2A[i].jbmg);
        printf("Registarski broj: ");
        printf("%d \n",niz2A[i].regBroj);
        printf("Izmjenjena neto plata: ");
        printf("%lf \n",niz2A[i].netoPlata);
    }
    //************
    //PRETRAZIVANJE
    for(i=0;i<BR;i++){
        kljuc=niz2[i].regBroj;
        index=pretrazivanjeBinarno(kljuc,niz2A,0,BR-1);

        if(index==-1)
            printf("Zaposlenik sa maticnim brojem %d nije pronadjen \n",kljuc);
        else
            if(niz2[i].netoPlata==niz2A[index].netoPlata)
                printf("Neto plata za zaposlenika sa maticnim brojem %d je nepromjenjena \n",kljuc);
            else{
                niz2[i].netoPlata=niz2A[index].netoPlata;
                printf("Neto plata za zaposlenika sa maticnim brojem %d je uspješno izmjenjena \n",kljuc);
            }


    }

    dat= fopen("Zaposlenici.dat","wb");
    fwrite(&niz2[0],sizeof(niz2),1,dat);
    fclose(dat);

    return 0;
}