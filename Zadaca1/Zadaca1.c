#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BR 15

 typedef struct zaposlenici{
   char JMBG[14];
   char broj_licne_karte[10];
   char prezime_ime[41];
   int registarski_broj;
   int zaposlen_na_neodredjeno;
   float neto_plata;
};
typedef struct zaposlenici_azurirano{
   char JMBG[14];
   int registarski_broj;
   float izmjenjena_neto_plata;
};
void zamijeni_zaposlenici(struct zaposlenici *pok1,struct zaposlenici *pok2){
    struct zaposlenici temp;
    temp = *pok1;
    *pok1=*pok2;
    *pok2=temp;
}
void zamijeni_zaposlenici_azurirano(struct zaposlenici_azurirano *pok1,struct zaposlenici_azurirano *pok2){
    struct zaposlenici_azurirano temp;
    temp = *pok1;
    *pok1=*pok2;
    *pok2=temp;
}
void sortirajBubble(struct zaposlenici niz[],int n){
    int i,j,sortiran;

    for(i=n;i>0;i--){
        sortiran=1;
        for(j=1;j<i;j++){
            if(niz[j-1].prezime_ime>niz[j].prezime_ime){
                zamijeni_zaposlenici(&niz[j-1],&niz[j]);
                sortiran=0;
                }
        }

        if(sortiran)
            break;
    }
}
void sortirajSelection(struct zaposlenici_azurirano niz[],int n){
    int i,j;

    for(i=1;i<n;i++){
        j=i;
        while(j>0 && niz[j-1].registarski_broj>niz[j].registarski_broj){
            zamijeni_zaposlenici_azurirano(&niz[j-1],&niz[j]);
            j--;
            }
    }

}
int pretrazivanjeBinarno(int kljuc, struct zaposlenici_azurirano niz[],int donji, int gornji){
    int index=-1,sredina;

     while(donji<=gornji){
        sredina=(donji+gornji)/2;
        if(niz[sredina].registarski_broj==kljuc){
            index=sredina;
            return index;
        }else if(niz[sredina].registarski_broj<kljuc){
            donji=sredina+1;
        }else{
            gornji=sredina-1;
        }
    }

    return index;
}

int main()
{
    struct zaposlenici niz[BR];
    struct zaposlenici_azurirano nizA[BR];
    int i,donji,gornji,sredina,index,kljuc;
    FILE *dat;
    //UCITAVANJE BINARNE DATOTEKE Zaposlenici.dat U niz
    dat= fopen("Zaposlenici.dat","rb");
    fread(&niz[0],sizeof(niz),1,dat);
    fclose(dat);
    //UCITAVANJE DATOTEKE 'Zaposlenici-Azurirano.txt U nizA'
    dat= fopen("Zaposlenici-Azurirano.txt","r");
    fread(&nizA[0],sizeof(nizA),1,dat);
    fclose(dat);

    //ISPIS PRVOG NIZA
    printf("\n\nIspis datoteke Zaposlenici.dat \n\n");
    for(i=0;i<BR;i++){
        printf("Ispis %d. zaposlenika \n",i+1);
        printf("JBMG: ");
        printf("%s \n",niz[i].JMBG);
        printf("Broj licne: ");
        printf("%s \n",niz[i].broj_licne_karte);
        printf("Prezime i ime): ");

        printf("%s \n",niz[i].prezime_ime);

        printf("Registarski broj: ");
        printf("%d \n",niz[i].registarski_broj);
        printf("Da li je zaposlen na neodredjeno (1 da, 0 ne) ");
        printf("%d \n",niz[i].zaposlen_na_neodredjeno);
        printf("Neto plata: ");
        printf("%lf \n",niz[i].neto_plata);
    }
    printf("\n\nIspis datoteke Zaposlenici-Azurirano.txt \n\n");
    for(i=0;i<BR;i++){
        printf("Ispis %d. zaposlenika \n",i+1);
        printf("JBMG: ");
        printf("%s \n",nizA[i].JMBG);
        printf("Registarski broj: ");
        printf("%d \n",nizA[i].registarski_broj);
        printf("Izmjenjena neto plata: ");
        printf("%lf \n",nizA[i].izmjenjena_neto_plata);
    }
    //SORTIRANJE niz i nizA
    sortirajBubble(niz,BR);
    sortirajSelection(nizA,BR);
    //ISPIS NAKON SORTIRANJA
    /*printf("\n\nIspis datoteke Zaposlenici.dat \n\n");
    for(i=0;i<BR;i++){
        printf("Ispis %d. zaposlenika \n",i+1);
        printf("JBMG: ");
        printf("%s \n",niz[i].JMBG);
        printf("Broj licne: ");
        printf("%s \n",niz[i].broj_licne_karte);
        printf("Prezime i ime): ");

        printf("%s \n",niz[i].prezime_ime);

        printf("Registarski broj: ");
        printf("%d \n",niz[i].registarski_broj);
        printf("Da li je zaposlen na neodredjeno (1 da, 0 ne) ");
        printf("%d \n",niz[i].zaposlen_na_neodredjeno);
        printf("Neto plata: ");
        printf("%lf \n",niz[i].neto_plata);
    }*/
    printf("\n\nIspis datoteke Zaposlenici-Azurirano.txt nakon sortiranja \n\n");
    for(i=0;i<BR;i++){
        printf("Ispis %d. zaposlenika \n",i+1);
        printf("JBMG: ");
        printf("%s \n",nizA[i].JMBG);
        printf("Registarski broj: ");
        printf("%d \n",nizA[i].registarski_broj);
        printf("Izmjenjena neto plata: ");
        printf("%lf \n",nizA[i].izmjenjena_neto_plata);
    }
    //************
    //PRETRAZIVANJE
    for(i=0;i<BR;i++){
        kljuc=niz[i].registarski_broj;
        index=pretrazivanjeBinarno(kljuc,niz,0,BR-1);

        if(index==-1)
            printf("Zaposlenik sa maticnim brojem %d nije pronadjen \n",kljuc);
        else
            if(niz[i].neto_plata==nizA[index].izmjenjena_neto_plata)
                printf("Neto plata za zaposlenika sa maticnim brojem %d je nepromjenjena \n",kljuc);
            else{
                niz[i].neto_plata=nizA[index].izmjenjena_neto_plata;
                printf("Neto plata za zaposlenika sa maticnim brojem %d je uspješno izmjenjena \n",kljuc);
            }


    }

    dat= fopen("Zaposlenici.dat","wb");
    fwrite(&niz[0],sizeof(niz),1,dat);
    fclose(dat);

    return 0;
}