#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define BR 20

int *stvori_polje(int a, int b, int n){
    int i;
    int *polje=(int *)malloc(sizeof(int)*n);
    srand(time(0));

    for(i=0;i<n;i++)
        polje[i]=rand()%(b-a+1)+a;

    return polje;
}
void ispisi(int *niz,int n){
    int i;
    for(i=0;i<n;i++)
        printf("%d ",niz[i]);
    printf("\n");
}

int *izbaci_parne(int *polje, int *br_clanova){
    int i=0,j=0;
    while(i<*br_clanova){
        if(polje[i]%2){//neparan
            if(i==j){   //ako su svi neparni samo ce proci kroz niz
                i++;j++;
            }else{
                polje[j]=polje[i];
                i++;j++;
            }
        }else
            i++;
    }

    if(i==j)//svi elementi u polju su neparni i nema potrebe za dealokaciju
        return polje;
    else{
        *br_clanova=j;
        polje=(int *)realloc(polje,sizeof(int)*j);
        return polje;
    }
}
int main()
{
    {   //Zadatak1
        int *polje,i;
        polje=stvori_polje(10,12,BR);
        printf("Zadatak 1 \n");
        ispisi(polje,BR);

        free(polje);
    }
    {   //Zadatak2
        int i,br_clanova=BR;
        int *polje=(int*)malloc(sizeof(int)*br_clanova);
        polje=stvori_polje(1,9,br_clanova);
        printf("Zadatak 2 \n");
        ispisi(polje,br_clanova);

        polje=izbaci_parne(polje,&br_clanova);
        ispisi(polje,br_clanova);

        free(polje);
    }

    return 0;
}
