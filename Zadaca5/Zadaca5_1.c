#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int vrijednost;
    struct Cvor* naredni;
}Cvor;
typedef struct{
    Cvor *glava;
    int velicina;
}Stog;

void init_cvor(Cvor *cvor,int vrijednost);
void init_stog(Stog *stog);
int dodaj(int element,Stog *stog);
int skini (int *element, Stog *stog);
int vrh(Stog* stog);
int prazan(Stog* stog);

Stog spoji(Stog *stog1, Stog *stog2);

int main()
{
    int i;
    Stog stog1,stog2,stog;
    init_stog(&stog1);
    init_stog(&stog2);
    init_stog(&stog);

    for(i=5;i>1;i--){
        dodaj(i,&stog1);
        dodaj(i+1,&stog2);
    }

    stog=spoji(&stog1,&stog2);
    printf("Vrh stoga1 je:%d\nVrh stoga2 je:%d\nVrh stoga je %d\n",vrh(&stog1),vrh(&stog2),vrh(&stog));

    printf("\nStidanje sa stoga1 \n");
    while(skini(&i,&stog1)){
        printf("%d ",i);
    }

    printf("\nStidanje sa stoga2 \n");
    while(skini(&i,&stog2)){
        printf("%d ",i);
    }

    printf("\nStidanje sa stoga \n");
    while(skini(&i,&stog)){
        printf("%d ",i);
    }

    return 0;
}
void init_cvor(Cvor *cvor,int vrijednost){
    cvor->naredni=NULL;
    cvor->vrijednost=vrijednost;
}
void init_stog(Stog *stog){
    stog->glava=NULL;
    stog->velicina=0;
}
int dodaj(int element,Stog *stog){
    Cvor *c=(Cvor*)malloc(sizeof(Cvor));
    init_cvor(c,element);
    if(stog->glava==NULL){
        stog->glava=c;
        stog->velicina++;
        return 1;
    }else{
        c->naredni=stog->glava;
        stog->glava=c;
        stog->velicina++;
        return 1;
    }
    return 0;
}
int skini (int *element, Stog *stog){
    Cvor *temp=stog->glava;
    if(temp==NULL)
        return 0;

    *element=temp->vrijednost;
    stog->glava=stog->glava->naredni;
    free(temp);
    return 1;
}
int vrh(Stog* stog){
    return stog->glava->vrijednost;
}
int prazan(Stog* stog){
    return (stog->glava==NULL);
}
Stog kopiraj(Stog *original){
    Stog tempStog;
    Cvor *tempCvor;
    init_stog(&tempStog);
    tempCvor=original->glava;
    while(tempCvor){
        dodaj(tempCvor->vrijednost,&tempStog);
        tempCvor=tempCvor->naredni;
    }
    return tempStog;
}
Stog spoji(Stog* stog1,Stog* stog2){
    Stog temp1=kopiraj(stog1),temp2=kopiraj(stog2);
    temp1=kopiraj(&temp1);temp2=kopiraj(&temp2);
    //Zbog osobine stoga, jedan poziv funkcije kopiraj, kopira elemente stoga u obrnutom redosljedu
    Stog temp;
    init_stog(&temp);

    int elem;

    while(!prazan(&temp1) && !prazan(&temp2)){
        if(vrh(&temp1)<vrh(&temp2))
            skini(&elem,&temp1);
        else
            skini(&elem,&temp2);

        dodaj(elem,&temp);

    }
    while(!prazan(&temp1)){
        skini(&elem,&temp1);
        dodaj(elem,&temp);
    }
    while(!prazan(&temp2)){
        skini(&elem,&temp2);
        dodaj(elem,&temp);
    }
    return temp;
}
