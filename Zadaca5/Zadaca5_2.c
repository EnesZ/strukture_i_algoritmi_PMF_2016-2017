#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int vrijednost;
    struct Cvor* naredni;
}Cvor;
typedef struct{
    Cvor *glava;
    int velicina;
}Stog;
typedef struct{
    Cvor *glava;
    Cvor *rep;
    int velicina;
}Red;

void init_cvor(Cvor *cvor,int vrijednost);
void init_stog(Stog *stog);
int dodaj(int element,Stog *stog);
int skini (int *element, Stog *stog);

void init_red(Red *red);
int dodaj_u_red (int element, Red *red);
int skini_iz_reda (int *element, Red *red);

int sadrzi(Red *red, int element);
void izbaci_duplikate(Red* red){
    int elem;
    Red pomocni;
    init_red(&pomocni);
    Cvor *c=red->glava,*kraj=red->rep;
    if(red->glava==NULL)
        return;

    do{//Jednim prolaskom kroz red uklanjamo sve duplikate
        if(!sadrzi(&pomocni,c->vrijednost)){
            dodaj_u_red(c->vrijednost,&pomocni);//Ako pomocni red ne sadrzi element, dodajemo ga u pomocni

            dodaj_u_red(c->vrijednost,red);//Element koji je pregledan odlazi na kraj reda
            c=c->naredni;
            skini_iz_reda(&elem,red);
        }else{//Ako pomocni red sadrzi element, to znaci da smo takav element vec pregledali i on se brise
            c=c->naredni;
            skini_iz_reda(&elem,red);

        }
    }while(c!=kraj);
    c=red->glava->naredni;
    if(red->glava->vrijednost==c->vrijednost)
        skini_iz_reda(&elem,red);
}
void ispisi(Red *red){
    Cvor *c=red->glava;

    while(c){
        printf("%d ",c->vrijednost);
        c=c->naredni;
    }
}
int main()
{
    Red red;
    init_red(&red);
    int i;

    dodaj_u_red(6,&red);

    for(i=0;i<5;i++){
        dodaj_u_red(i,&red);
        dodaj_u_red(i+1,&red);
        dodaj_u_red(i+2,&red);
    }
    printf("Red prije izbacivanja duplikata \n");
    ispisi(&red);
    printf("\n");
    izbaci_duplikate(&red);
    printf("Red poslije izbacivanja duplikata \n");
    ispisi(&red);

    return 0;
}
void init_cvor(Cvor *cvor,int vrijednost){
    cvor->naredni=NULL;
    cvor->vrijednost=vrijednost;
}
void init_stog(Stog *stog){
    stog->glava=NULL;
    stog->velicina=0;
}
int dodaj(int element,Stog *stog){
    Cvor *c=(Cvor*)malloc(sizeof(Cvor));
    init_cvor(c,element);
    if(stog->glava==NULL){
        stog->glava=c;
        stog->velicina++;
        return 1;
    }else{
        c->naredni=stog->glava;
        stog->glava=c;
        stog->velicina++;
        return 1;
    }
    return 0;
}
int skini (int *element, Stog *stog){
    Cvor *temp=stog->glava;
    if(temp==NULL)
        return 0;

    *element=temp->vrijednost;
    stog->glava=stog->glava->naredni;
    free(temp);
    return 1;
}
void init_red(Red *red){
    red->glava=NULL;
    red->rep=NULL;
    red->velicina=0;
}
int dodaj_u_red (int element, Red *red){
    Cvor *c = (Cvor*)malloc(sizeof(Cvor));
    init_cvor(c,element);

    if(red->rep==NULL){//prazan red
        red->rep=red->glava=c;
        red->velicina++;
        return 1;
    }else{
        red->rep->naredni=c;
        red->rep=c;
        red->velicina++;
        return 1;
    }
    return 0;
}
int skini_iz_reda (int *element, Red *red){
    Cvor *temp;
    if(red->glava==NULL)
        return 0;
    *element=red->glava->vrijednost;
    temp=red->glava;
    red->glava=red->glava->naredni;
    free(temp);
    return 1;
}
int sadrzi(Red *red, int element){
    Cvor* temp=red->glava;
    while(temp){
        if(temp->vrijednost==element)
            return 1;
        temp=temp->naredni;
    }
    return 0;
}
