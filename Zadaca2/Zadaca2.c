#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define BR 100

int BR_IT_M3=0,BR_IT_QL=0;

//funkcija swap + broji iteracije odredjene funkcije
void zamijeni(int *p1,int *p2,char mod){
    int temp;
    temp=*p1;
    *p1=*p2;
    *p2=temp;
    if(mod=='M')
        BR_IT_M3++;
    else if(mod=='L')
        BR_IT_QL++;
}
//Pomocna funkcija
void ispisi(int A[],int n){
    int i;
    for(i=0;i<n;i++)
        printf("%d ",A[i]);
    printf("\n");
}
//Bubble sort sluzi samo za sortiranje podniza od 3 elementa kod QSortMedian
void bubbleSort(int A[],int lijevo,int desno){
    int i,j;

    for(i=desno;i>lijevo-1;i--){
        for(j=lijevo;j<=i;j++){
            if(A[j]>A[j+1])
                zamijeni(&A[i],&A[j],'M');
        }
    }

}
/*          QuickSort
    Korak 1:
        Funkcija QSort
            Postavljamo pivot na svoje mjesto u nizu pomocu funkcije particija
            Dijelimo niz na dva dijela, podniz sa lijeve strane i podniz sa desne strane
            Podnizove sortiramo funkcijom QSort
    Korak 2:
        Funkcija particija
            Postavlja elemente niza na lijevu ili na desno stranu u odnosu na pivota
            Postavlja povota na svoje mjesto
            Vraca index mjesta na kojem je postavljen pivot
*/
int particija(int A[],int lijevo,int desno){
    int donji,gornji,pivot;
    donji=lijevo;
    gornji=desno;
    pivot=A[desno];

    while(donji<gornji){
        //lijevi "pokazivac"
        //sve dok su elementi na koje pokazuje lijevi pokazivac manji od pivota, lijevi pokazivac pomjeramo udesmo
        while((donji< gornji) && A[donji]<pivot)
            donji++;
        //desni "pokazivac"
        //sve dok su elementi na koje pokazuje desni pokazivac veci ili jednaki od pivota, desni pokazivan pomjeramo ulijevo
        while( (donji< gornji) && A[gornji]>=pivot)
            gornji--;

        //zamjena elemenata na kojima su se zaustavili pokazivaci
        if(donji<gornji){

            zamijeni(&A[donji],&A[gornji],'M');
            }
    }
    //zamjena pivota i elementa na kojem su se poklopili pokazivaci
    A[desno]=A[donji];
    A[donji]=pivot;
    BR_IT_QL++;
    //vracamo index gdje je postavljen pivot
    //pivot je na svom mjestu u nizu

    return donji;
}
//particija i particija1  se razlikuju samo kod funkcije zamijeni zbog potrebe brojenja iteracija
int particija1(int A[],int lijevo,int desno){
    int donji,gornji,pivot;
    donji=lijevo;
    gornji=desno;
    pivot=A[desno];

    while(donji<gornji){
        //lijevi "pokazivac"
        //sve dok su elementi na koje pokazuje lijevi pokazivac manji od pivota, lijevi pokazivac pomjeramo udesmo
        while((donji< gornji) && A[donji]<pivot)
            donji++;
        //desni "pokazivac"
        //sve dok su elementi na koje pokazuje desni pokazivac veci ili jednaki od pivota, desni pokazivan pomjeramo ulijevo
        while( (donji< gornji) && A[gornji]>=pivot)
            gornji--;

        //zamjena elemenata na kojima su se zaustavili pokazivaci
        if(donji<gornji){

            zamijeni(&A[donji],&A[gornji],'L');
            }
    }
    //zamjena pivota i elementa na kojem su se poklopili pokazivaci
    A[desno]=A[donji];
    A[donji]=pivot;
    BR_IT_QL++;
    //vracamo index gdje je postavljen pivot
    //pivot je na svom mjestu u nizu

    return donji;
}
int medijan3(int A[], int lijevo, int desno){
    int k=(lijevo+desno)/2;
    if(A[lijevo]>A[k])
        zamijeni(&A[lijevo],&A[k],'M');
    if(A[lijevo]>A[desno])
        zamijeni(&A[lijevo],&A[desno],'M');
    if(A[k]>A[desno])
        zamijeni(&A[k],&A[desno],'M');

    zamijeni(&A[k],&A[desno-1],'M');

    return particija(A,lijevo,desno-1);
}
void QSortMedian(int A[], int lijevo, int desno){
    int pivot;

    if(desno-lijevo>2){

        pivot=medijan3(A,lijevo,desno);
        QSortMedian(A,lijevo,pivot-1);
        QSortMedian(A,pivot+1,desno);

    }else{
        bubbleSort(A,lijevo,desno);

    }

}
void QSortLastElement(int A[], int lijevo, int desno){
    int pivot;
    if(desno>lijevo){
        pivot=particija1(A,lijevo,desno);
        QSortLastElement(A,lijevo,pivot-1);
        QSortLastElement(A,pivot+1,desno);
    }

}
typedef struct rezultat{

    int A[BR];
    int broj_iteracija;
};
int main()
{
    FILE *p;
    int i;
    struct rezultat rezultati[2];
    //Generisanje niza slucajnih brojeva od 0 do 50
    printf("Nesortiran niz \n");
    srand(time(0));
    for(i=0;i<BR;i++){
        rezultati[0].A[i]=rezultati[1].A[i]=rand()%50;
        printf("%d ",rezultati[0].A[i]);
    }


    QSortMedian(rezultati[0].A,0,BR-1);
    QSortLastElement(rezultati[1].A,0,BR-1);
    printf("\nSortirani nizovi \n");
    ispisi(rezultati[0].A,BR);
    ispisi(rezultati[1].A,BR);

    /*for(i=0;i<BR;i++){
        rezultati[0].A[i]=niz[i];
        rezultati[1].A[i]=niz1[i];
    }*/
    rezultati[0].broj_iteracija=BR_IT_M3;
    rezultati[1].broj_iteracija=BR_IT_QL;

    p=fopen("Rezultat.dat","wb");
    fwrite(&rezultati[0],sizeof(rezultati),1,p);
    fclose(p);


    printf("\n\nBroj iteracija potreban za QSort median je: %d\nBroj iteracija potreban za QSort last element  %d",BR_IT_M3,BR_IT_QL);
    return 0;
}
