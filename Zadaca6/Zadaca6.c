#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct {
    char naziv[20];
    int cijena;

}Element;

typedef struct{
    Element element;
    struct Cvor *lijevo, *desno;
}Cvor;
typedef struct{
    Cvor *korijen;
}Stablo;
int max_cijena(Cvor *korijen){
    if(korijen==NULL){
        printf("Stablo nema elemenata \n");
        return 0;
    }
    Element elem=korijen->element;
    static int maxCijena;
    maxCijena=elem.cijena;

    void pronadji_max(Cvor *korijen){
        if(korijen!=NULL){
            Element elem=korijen->element;
            if(elem.cijena>maxCijena)
                maxCijena=elem.cijena;
            pronadji_max(korijen->desno);
            pronadji_max(korijen->lijevo);
        }
    }

    pronadji_max(korijen);
    return maxCijena;
}
int ukupna_cijena(Cvor *korijen){
    if(korijen==NULL){
        printf("Stablo nema elemenata \n");
        return 0;
    }
    static int suma=0;

    void sumiraj(Cvor *korijen){
        if(korijen!=NULL){
            Element elem=korijen->element;
            suma+=elem.cijena;
            sumiraj(korijen->desno);
            sumiraj(korijen->lijevo);
        }
    }

    sumiraj(korijen);
    return suma;
}
void cvor_init(Cvor *cvor,Element elem){
    cvor->desno=cvor->lijevo=NULL;
    cvor->element=elem;
}
void stablo_init(Stablo *stablo){
    stablo->korijen=NULL;
}
Cvor* ubaci(Cvor *korijen,Element elem){
    if(korijen==NULL){
        korijen=(Cvor*)malloc(sizeof(Cvor));
        cvor_init(korijen,elem);
    }else if(elem.cijena<korijen->element.cijena)
        korijen->lijevo=ubaci(korijen->lijevo,elem);
    else
        korijen->desno=ubaci(korijen->desno,elem);
    return korijen;

}
int main()
{

    Element elem1,elem2,elem3,temp;
    Cvor *c;
    elem1.cijena=70;
    strcpy(elem1.naziv,"elem1");
    elem2.cijena=120;
    strcpy(elem2.naziv,"elem2");
    elem3.cijena=90;
    strcpy(elem3.naziv,"elem3");

    Stablo stablo;
    stablo_init(&stablo);

    stablo.korijen=ubaci(stablo.korijen,elem1);
    stablo.korijen=ubaci(stablo.korijen,elem2);
    stablo.korijen=ubaci(stablo.korijen,elem3);

    printf("Maksimalna cijena u stablu je %d \n",max_cijena(stablo.korijen));
    printf("Ukupna cijena svih artikala je %d \n",ukupna_cijena(stablo.korijen));
    return 0;
}
