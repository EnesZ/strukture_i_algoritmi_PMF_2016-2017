#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct zapis {
    char imeprezime[30+1];
    int matbr;
    int godisnji;
}Zapis;

typedef struct at {
    struct zapis element;
    struct at *sljed;
}atom;

typedef struct lista{
    atom *glava;
    atom *trenutni_atom;
    int velicina_liste;
}Lista;

atom* atom_init(Zapis zapis){
    atom* a =(atom*)malloc(sizeof(atom));
    a->element=zapis;
    a->sljed=NULL;
    return a;
}
void lista_init(Lista* lista){
    lista->glava=NULL;
    lista->velicina_liste=0;
    lista->trenutni_atom=NULL;
}
void ispisi(atom *a){
    printf("Lista radnika: \n");
    if(a==NULL)
        printf("Nema radnika ! \n");
    while(a){
        Zapis zapis = a->element;

        printf("Ime i prezime:\n%s\nBroj dana godisnjeg odmora:%d \nMBR: %d\n",zapis.imeprezime,zapis.godisnji,zapis.matbr);
        printf("-----------------------------\n");
        a=a->sljed;
    }
    printf("Kraj \n");
}
int dodaj_iza(Lista *lista,Zapis zapis){
    atom *a= atom_init(zapis);
    if(lista->velicina_liste){
        a->sljed=lista->trenutni_atom->sljed;
        lista->trenutni_atom->sljed=a;
        lista->trenutni_atom=a;
    }else{
        lista->glava=a;
        lista->trenutni_atom=a;
    }
        ++(lista->velicina_liste);

}
void lista_dealociraj(Lista * lista)
{
    atom *temp, * cvor = lista->glava;
    while(cvor)
    {
        temp = cvor->sljed;
        free(cvor);
        cvor = temp;
    }
    lista->glava = lista->trenutni_atom = NULL;
    lista->velicina_liste = 0;

}
void lista_brisi(Lista *lista)
{
    if(lista->velicina_liste == 0)
    {
        printf("Greska: Nema elemenata za brisanje");
        return 0;
    }
    else
    {
        if(lista->velicina_liste==1){
            lista_dealociraj(lista);
            return 1;
        }
        atom *remove=lista->trenutni_atom;
        atom *temp = lista->glava;
        if(temp != lista->trenutni_atom){
            while(temp->sljed != lista->trenutni_atom)
            {
                temp = temp->sljed;
            }
        }
        if(lista->trenutni_atom==lista->glava){
            lista->trenutni_atom=lista->trenutni_atom->sljed;
            lista->glava=lista->trenutni_atom;
        }else if(lista->trenutni_atom->sljed){
            lista->trenutni_atom=lista->trenutni_atom->sljed;
            temp->sljed=lista->trenutni_atom;
        }else{

            lista->trenutni_atom=temp;
            lista->trenutni_atom->sljed=NULL;
        }

        free(remove);
        (lista->velicina_liste)--;

        return 1;
    }
}
int lista_naprijed(Lista *l){
    if(l->trenutni_atom && l->trenutni_atom->sljed)
    {
        l->trenutni_atom = l->trenutni_atom->sljed;
        return 0;
    }
    else
    {
        printf("Greska: Ne mozemo nakon zadnjeg cvora. \n");
        return l;
    }
}
void izbaci(Lista* lista){
    //postavlja na trenutni atom glavu liste, i lista sve atome do kraja

    if(lista->velicina_liste){
        lista->trenutni_atom=lista->glava;
        do{
            Zapis z=lista->trenutni_atom->element;
            if(z.godisnji>30){
                lista_brisi(lista);
                if(lista->trenutni_atom==NULL)//lista se dealocirala
                    break;
            }else{

                if(lista_naprijed(lista)){
                    break;
                    }
            }

        }while(1);

    }else{
        printf("Lista je prazna \n");
    }

}
int main()
{
    Lista l;
    lista_init(&l);
    Zapis z1;
        strcpy(z1.imeprezime,"Enes Zvornicanin");
        z1.godisnji=40;
        z1.matbr=12345678;
    Zapis z2;
        strcpy(z2.imeprezime,"Ahmed Begic");
        z2.godisnji=40;
        z2.matbr=23456789;

    dodaj_iza(&l,z1);
    dodaj_iza(&l,z2);
    ispisi(l.glava);

    izbaci(&l);
    ispisi(l.glava);
    return 0;
}
