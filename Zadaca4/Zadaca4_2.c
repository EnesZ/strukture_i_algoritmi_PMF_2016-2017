#include <stdio.h>
#include <stdlib.h>

typedef int ElemTip;

typedef struct
{
    ElemTip element;
    struct Cvor * prethodni;
    struct Cvor * naredni;

} Cvor;

typedef struct
{
    Cvor * glava;
    Cvor * rep;
    Cvor * trenutni_cvor;
    int velicina_liste;
} Lista;

// Dodavanje novog cvora
Cvor * cvor_novi(ElemTip v)
{
    Cvor * c = (Cvor*) malloc(sizeof(Cvor));
    c->element = v;
    c->prethodni = NULL;
    c->naredni = NULL;
    return c;
}

void ispisi(Cvor *c)
{
    while(c != NULL)
    {
        printf("%d\n", c->element);
        c = c->naredni;
    }
    printf("KRAJ\n");
}

// Kreira se prazna lista
Lista* lista_init(Lista* lista)
{
    lista->glava = NULL;
    lista->rep = NULL;
    lista->trenutni_cvor = NULL;
    lista->velicina_liste = 0;
}

// Dealocira se postojeca lista
void lista_dealociraj(Lista * lista)
{
    Cvor *temp, * cvor = lista->glava;
    while(cvor)
    {
        temp = cvor->naredni;
        free(cvor);
        cvor = temp;
    }
    lista->glava = lista->trenutni_cvor = lista->rep = NULL;
    lista->velicina_liste = 0;
}

int lista_naprijed(Lista *l)
{
    if(l->trenutni_cvor && l->trenutni_cvor->naredni)
    {
        l->trenutni_cvor = l->trenutni_cvor->naredni;
        return 0;
    }
    else
    {
        printf("Greska: Ne mozemo nakon zadnjeg cvora.\n");
        return l;
    }
}

int lista_nazad(Lista *l)
{
    if(l->trenutni_cvor && l->trenutni_cvor->prethodni)
    {
        l->trenutni_cvor = l->trenutni_cvor->prethodni;
        return 0;
    }
    else
    {
        printf("Greska: Ne mozemo ispred prvog cvora.\n");
        return l;
    }
}

void lista_dodaj_iza(Lista *lista, ElemTip v)
{
    Cvor *novi = cvor_novi(v);
    Cvor *temp = NULL;

    if(lista->velicina_liste > 0)
    {
        novi->prethodni = lista->trenutni_cvor;
        novi->naredni = lista->trenutni_cvor->naredni;

        // lista->trenutni_cvor->naredni->prethodni
        temp = lista->trenutni_cvor->naredni;//jer kompajler javlja gresku bez ovoga
        if(temp != NULL)
            temp->prethodni = novi;

        lista->trenutni_cvor->naredni = novi;
        lista->trenutni_cvor = novi;
    }
    else
    {
        lista->glava = novi;
        lista->rep =novi;
        lista->trenutni_cvor = novi;
    }
    ++(lista->velicina_liste);
}

void lista_brisi(Lista *lista)
{
    if(lista->velicina_liste == 0)
    {
        printf("Greska: Nema elemenata za brisanje.\n");
        return 0;
    }
    else
    {
        if(lista->velicina_liste == 1)
        {
            lista_dealociraj(lista);
            return 1;
        }

        Cvor *ukloni = lista->trenutni_cvor;
        Cvor *temp = NULL;

        if(lista->trenutni_cvor == lista->glava)
        {
            lista->trenutni_cvor = lista->trenutni_cvor->naredni;
            lista->trenutni_cvor->prethodni = NULL;
            lista->glava = lista->trenutni_cvor;
        }
        else if(lista->trenutni_cvor->naredni)
        {
            lista->trenutni_cvor = lista->trenutni_cvor->naredni;
            lista->trenutni_cvor->prethodni = ukloni->prethodni;

            temp = ukloni->prethodni;
            temp->naredni = lista->trenutni_cvor;
        }
        else //trenutni je rep
        {
            lista->trenutni_cvor = ukloni->prethodni;
            lista->rep=lista->trenutni_cvor;

            temp = ukloni->prethodni;
            temp->naredni = NULL;

        }

        free(ukloni);
        --(lista->velicina_liste);
        return 1;
    }
}
Cvor* lista_trazi(Lista *lista,ElemTip v){
    if(lista->velicina_liste){
        Cvor *c=lista->glava;
        while(c!= NULL){
            if(c->element==v)
                return c;

            c=c->naredni;

        }
    }else{
        printf("Lista je prazna \n");
    }

    return NULL;
}
void lista_dodaj_ispred(Lista *lista, ElemTip v){
    Cvor *novi = cvor_novi(v);
    Cvor *temp = NULL;

    if(lista->velicina_liste > 0)
    {
        novi->prethodni = lista->trenutni_cvor->prethodni;
        novi->naredni = lista->trenutni_cvor;


        temp = lista->trenutni_cvor->prethodni;//jer kompajler javlja gresku bez ovoga
        if(temp != NULL)
            temp->naredni = novi;
        else// ispred glave
            lista->glava=novi;

        lista->trenutni_cvor->prethodni = novi;
        lista->trenutni_cvor = novi;
    }
    else
    {
        lista->glava = novi;
        lista->rep =novi;
        lista->trenutni_cvor = novi;
    }
    ++(lista->velicina_liste);




}
void main()
{
    int i;
    Cvor *c;
    Lista l;
    lista_init(&l);

    for(i=1;i<5;i++)
        lista_dodaj_ispred(&l,i);

    ispisi(l.glava);


    printf("Trazimo cvor sa elementom %d \n",3);
    c= lista_trazi(&l,3);
    if(c!=NULL)
        printf("Pronadjen cvor %d \n",c->element);
    else
        printf("Cvor nije pronadjen \n");

    printf("Trazimo cvor sa elementom %d \n",7);
    c=lista_trazi(&l,7);
    if(c!=NULL)
        printf("Pronadjen cvor %d \n",c->element);
    else
        printf("Cvor nije pronadjen \n");

    lista_nazad(&l);
    lista_dodaj_iza(&l,43);

    ispisi(l.glava);

    lista_nazad(&l);
    lista_nazad(&l);

    lista_dealociraj(&l);
    return  0;

}
